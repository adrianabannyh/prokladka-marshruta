﻿#pragma once
#include "stdafx.h"
#include "math.h"
#include "Form1.h"

int EndSelect(double za[25],double zb[25],double px[25],double py[25],int nmbmax,int nmbp,
                    double rmin,double rmax,double alf);
int EndMarsh(double zx[25],double zy[25],double rmin,double rmax,double rlen,double alf,
	          int indvar,int ipar[4],int nmbmax);
double myAngle(double nx,double ny,double fx,double fy);
double myAngleP(double nx,double ny);
double myMod(double nx,double ny,double fx,double fy);
double myArg(double nx,double ny,double fx,double fy);
int RegBoundary(double zx[25],double zy[25],double px[25],double py[25],int nmb,int nmbp,int indreg);



int fact1(int mmm,int xm[25],int ym[25])
{
    	         int i;
                 System::Windows::Forms::PaintEventArgs^ e;
	             for( i=0;i<mmm;i++)
				 {
				 e->Graphics->DrawLine(System::Drawing::Pens::Green, xm[i], ym[i], xm[i+1], ym[i+1]);
				 }
				 return mmm+i;
 }


int fact(int xm[25],int ym[25])
{
// Для демонстрации
int nmbmax,nmbp,k,nn;
double za[25],zb[25],px[25],py[25];
double alf,rmin,rmax,Pi;
Pi=atan(1.)*4;
	  za[0]=4.5;
	  zb[0]=5.0;
	  za[1]=-1.;
	  zb[1]=0.;
	  za[2]=0.;
	  zb[2]=0.;
	  za[3]=0.;
	  zb[3]=0.;
	  za[4]=2.;
	  zb[4]=2.;
	  za[5]=0.8;
	  zb[5]=-0.6;
alf=Pi/3;
rmin=1;
rmax=100;
nmbmax=25;
nmbp=4;
px[0]=0;
py[0]=0;
px[1]=10;
py[1]=0;
px[2]=10;
py[2]=10;
px[3]=0;
py[3]=10;
nn=EndSelect(za,zb,px,py,nmbmax,nmbp,rmin,rmax,alf);
  for (k=0;k<nn;k++)
  {
	  xm[k]=za[k]*150.;
	  ym[k]=900.-zb[k]*150.;
  }
	  return nn;
}




int EndMarsh(double zx[25],double zy[25],double rmin,double rmax,double rlen,double alf,
	          int indvar,int ipar[4],int nmbmax)
{
//Функцияа предназначена для определения точек поворота между началом входного
//и концом конечного векторов при условии:
// - угол поворота < alf;
// - длина отрезка между точками поворота > rmin.
// Результат -число  точек поворота между началом входного и концом конечного векторов
//
// 
//Формальные  аргументы.
// При входе :
//   zx[0],zy[0] - координаты начала входного вектора
//   zx[1],zy[1] -предпочтительное направление входного вектора
//   zx[2],zy[2] -минимальное значение дипазона допустимых значений направления входного вектора
//   zx[3],zy[3] -максимальное значение дипазона допустимых значений направления входного вектора
//   zx[4],zy[4] - координаты конца конечергр вектора
//   zx[5],zy[5] -направление конечного вектора
// alf - максимальный угол поворота
// rmin - минимальное расстояние между точками поворота
// rmax -максимально-допустимая длина ломаной
// indvar - номер варианта касательных
// nmbmax -максимально-допустимое число точек ломаной
// 
// При выходе
//	zx[i],zy[i] - координаты точки поворота с номером i
//  (точки нумеруются начиная с начала ломаной до ее конца).
// При этом :
//   zx[0],zy[0] - координаты начала входного вектора
//   zx[nmb-1,zy[nmb-1] -координаты конца конечного вектора
// где nmb - общее число точек ломаной, включая точки начала и конца.
// rlen - длина ломаной
// ipar - 
// ipar[0] - признак существования решения
// ipar[1] - общее число точек ломаной
// ipar[2] - признак наличия петли
// ipar[3] - признак наличия отрезка с длиной меньшей,чем rmin
double knx,kny,kfx,kfy,onx,ony,ofx,ofy;
double  nx1,ny1,nx2,ny2,fx1,fy1,fx2,fy2;
double r,rr,d,d1,fin,fout,Pi,beta,gamm;
int inds,nmb,indloop,indr;
int nmb1,nmb2,i;
Pi=atan(1.)*4;
nx1=zx[0];
ny1=zy[0];
// конец входного вектора 
fin=myAngleP(zx[1],zy[1]);
nx2=nx1+rmin*cos(fin);
ny2=ny1+rmin*sin(fin);
//начало финального вектора
fx2=zx[4];
fy2=zy[4];
fout=myAngleP(zx[5],zy[5]);
fx1=fx2-rmin*cos(fout);
fy1=fy2-rmin*sin(fout);
 for (i=0;i<25;i++)
  {
	  zx[i]=0.;
	  zy[i]=0.;
  }
//определение центров окужностей
r=rmin/(2*sin(alf/2));
if((indvar==1)||(indvar==2)){
	gamm=fin+Pi*0.5+alf/2;
}
else{
	gamm=fin-Pi*0.5-alf/2;
}
onx=nx2+r*cos(gamm);
ony=ny2+r*sin(gamm);
if((indvar==1)||(indvar==3)){
	gamm=fin+Pi*0.5+alf/2;
}
else{
	gamm=fin-Pi*0.5-alf/2;
}
ofx=fx2+r*cos(gamm);
ofy=fy2+r*sin(gamm);
//определение существования решений
if((indvar==1)||(indvar==4)){
    inds=0;
}
else{
	d=myMod(onx,ony,ofx,ofy);
	if((d-2*r)<0) inds=1;
	else inds=0;
}
//определение точек касания
if(inds==0){
  gamm=myAngle(ofx,ofy,onx,ony);
  if(indvar==1){
    d=-Pi*0.5+gamm;
	d1=d;
  }
  if(indvar==4){
    d=Pi*0.5+gamm;
	d1=d;
  }
  if(indvar==2){
    rr=sqrt((ofx-onx)*(ofx-onx)+(ofy-ony)*(ofy-ony));
	beta=asin((2*r)/rr);
    d=gamm+beta-Pi*0.5;
	d1=gamm+beta+Pi*0.5;
  }
  if(indvar==1){
    rr=sqrt((ofx-onx)*(ofx-onx)+(ofy-ony)*(ofy-ony));
	beta=asin((2*r)/rr);
    d=gamm+beta+Pi*0.5;
	d1=gamm+beta-Pi*0.5;
  }
knx=onx+r*cos(d);
kny=ony+r*sin(d);
kfx=ofx+r*cos(d1);
kfy=ofy+r*sin(d1);
//определение точек поворота  
gamm=myArg(knx,kny,kfx,kfy);
if((indvar==1)||(indvar==2)){
	d=gamm-fin;
	d1=alf;
}
else{
	d=fin-gamm;
	d1=-alf;
}
if(d>2*Pi) d=d-2*Pi;
if(d<0) d=d+2*Pi;
nmb1=d/alf;
zx[0]=nx1;
zy[0]=ny1;
zx[1]=nx2;
zy[1]=ny2;
if(nmb1>0)
{
  for (i=1;i<nmb1+1;i++)
  {
        d=i*d1+fin;
        zx[i+1]=zx[i]+rmin*cos(d);
        zy[i+1]=zy[i]+rmin*sin(d);
  }
}
if((indvar==1)||(indvar==3)){
	d=fout-gamm;
	d1=alf;
}
else{
	d=gamm-fout;
	d1=-alf;
}
if(d>2*Pi) d=d-2*Pi;
if(d<0) d=d+2*Pi;
nmb2=d/alf;
zx[nmb1+nmb2+2]=fx1;
zy[nmb1+nmb2+2]=fy1;
zx[nmb1+nmb2+3]=fx2;
zy[nmb1+nmb2+3]=fy2;
if(nmb2>0)
{
  for (i=1;i<nmb2+1;i++)
  {
        d=-i*d1+fout-Pi;
        zx[nmb1+nmb2+2-i]=zx[nmb1+nmb2+3-i]+rmin*cos(d);
        zy[nmb1+nmb2+2-i]=zy[nmb1+nmb2+3-i]+rmin*sin(d);
  }
}
//
nmb=nmb1+nmb2+4;
d=myMod(zx[nmb1+1],zy[nmb1+1],zx[nmb1+2],zy[nmb1+2]);
if(d<rmin){indr=1;}
if((nmb1+1)>(2*Pi/alf)){indloop=1;}
if((nmb2+1)>(2*Pi/alf)){indloop=1;}
rlen=0;
for (i=1;i<nmb;i++)
{
      d=zy[i]-zy[i-1];
      d1=zx[i]-zx[i-1];
      rr=sqrt(d*d+d1*d1);
      rlen=rlen+rr;
}
//==================
ipar[0]=inds;
ipar[1]=nmb;
ipar[2]=indloop;
ipar[3]=indr;
}
	return nmb;
}




int EndSelect(double za[25],double zb[25],double px[25],double py[25],int nmbmax,int nmbp,
                    double rmin,double rmax,double alf)
{
//Функцияа предназначена для определения лучшего варианта построения касательных для определения точек
//	поворота между началом входного и концом конечного векторов при условии:
// - угол поворота < alf;
// - длина отрезка между точками поворота > rmin.
// Результат -число  точек поворота в лучшем варианте началом входного и концом конечного векторов
//
// 
//Формальные  аргументы.
// При входе :
//   za[0],zb[0] - координаты начала входного вектора
//   za[1],zb[1] -предпочтительное направление входного вектора
//   za[2],zb[2] -минимальное значение дипазона допустимых значений направления входного вектора
//   za[3],zb[3] -максимальное значение дипазона допустимых значений направления входного вектора
//   za[4],zb[4] - координаты конца конечергр вектора
//   za[5],zb[5] -направление конечного вектора
// alf - максимальный угол поворота
// rmin - минимальное расстояние между точками поворота
// rmax -максимально-допустимая длина ломаной
// nmbmax -максимально-допустимое число точек ломаной
// px,py,nmbp -координаты вершин многоугольника, задающего район
// nmbp -число вершин многоугольника, задающего район
// При выходе
//	za[i],zb[i] - координаты точек поворота лучшего варианта
//  (точки нумеруются начиная с начала ломаной до ее конца).
// При этом :
//   za[0],zb[0] - координаты начала входного вектора
//   za[nmb-1,zb[nmb-1] -координаты конца конечного вектора
// где nmb - общее число точек ломаной, включая точки начала и конца.
double zx[25],zy[25];
double rlen,rl[5];
int  i,knmb,k,inds,indreg,indr,nmb,indloop,indvar,ipar[4];
int  iex[5],ireg[5],ir[5],inmb[5],iloop[5],nn,nk;
for (i=1;i<5;i++)
 {
for (k=0;k<7;k++)
	{
        zx[k]=za[k];
        zy[k]=zb[k];
     }
    indvar=i;
nn=EndMarsh(zx,zy,rmin,rmax,rlen,alf,indvar,ipar,nmbmax);
inds=ipar[0];
nmb=ipar[1];
indloop=ipar[2];
indr=ipar[3];
nk=RegBoundary(zx,zy,px,py,nmb,nmbp,indreg);
// сохранение inds,indreg,indr,nmb,indloop,rlen
    iex[i]=inds;
    ireg[i]=indreg;
    ir[i]=indr;
    inmb[i]=nmb;
    iloop[i]=indloop;
    rl[i]=rlen;
 }
// выбор допустимых вариантов по nmb с учетом inds,indreg,indr
for (i=1;i<5;i++)
 {
 if(iex[i]==0)
  {
      if(ireg[i]>0) iex[i]=0;
      if(ir[i]>0)  iex[i]=1;
  }
 }
// наилучший путь по nmb, при этом учет: indloop, rlen
nmb=25;
knmb=-1;
for (i=1;i<5;i++)
  {
	if(iex[i]==0)
	{	
        if(inmb[i]<nmb)
		{
            nmb=inmb[i];
            knmb=i;
		}
    }
 }
if(knmb>0)
 {
    indloop=iloop[knmb];
    rlen=rl[knmb];
 }
// Итоговый вариант
for (k=0;k<7;k++)
	{
        zx[k]=za[k];
        zy[k]=zb[k];
     }
    indvar=knmb;
nn=EndMarsh(zx,zy,rmin,rmax,rlen,alf,indvar,ipar,nmbmax);
nmb=ipar[1];
for (k=0;k<nmb;k++)
	{
        za[k]=zx[k];
        zb[k]=zy[k];
     }
	return nmb;
}




int RegBoundary(double zx[25],double zy[25],double px[25],double py[25],int nmb,int nmbp,int indreg)
{
int i,k;
double d1,d2,Pi;
Pi=atan(1.)*4;
indreg=0;
for (i=0;i<nmb;i++)
  {
  for (k=0;i<nmbp-1;i++)
  {
      d1=myArg(zx[i],zy[i],px[k],py[k]);
      d2=myArg(zx[i],zy[i],px[k+1],py[k+1]);
      d2=d2-d1;
      if(d2<0) d2=d2+2*Pi;
      if(d2>=Pi)indreg=indreg+1;
  }
  d1=myArg(zx[i],zy[i],px[nmbp-1],py[nmbp-1]);
  d2=myArg(zx[i],zy[i],px[0],py[0]);
  d2=d2-d1;
  if(d2<0)d2=d2+2*Pi;
  if(d2>=Pi)indreg=indreg+1;
  }
return indreg;
}



double myArg(double nx,double ny,double fx,double fy)
{
 double  d,dd,ddd,pi;
pi=atan(1.)*4;
d=fy-ny;
dd=fx-nx;
ddd=sqrt(d*d+dd*dd);
d=d/ddd;
dd=dd/ddd;
if((d>=0)&&(dd>=0)) ddd=asin(d);
if((d>=0)&&(dd<0)) ddd=pi-asin(d);
if((d<0)&&(dd<0)) ddd=pi-asin(d);
if((d<0)&&(dd>=0)) ddd=2*pi+asin(d);
return ddd;
}



double myMod(double nx,double ny,double fx,double fy)
//модуль вектора (nx,ny)-начало,(fx,fy)-конец)
{
double d,dd,ddd;
d=fy-ny;
dd=fx-nx;
ddd=sqrt(d*d+dd*dd);
return ddd;
}





double myAngle(double nx,double ny,double fx,double fy)
{
 double  d,dd,ddd,pi;
pi=atan(1.)*4;
d=ny-fy;
dd=nx-fx;
ddd=sqrt(d*d+dd*dd);
d=d/ddd;
dd=dd/ddd;
if((d>=0)&&(dd>=0)) ddd=asin(d);
if((d>=0)&&(dd<0)) ddd=pi-asin(d);
if((d<0)&&(dd<0)) ddd=pi-asin(d);
if((d<0)&&(dd>=0)) ddd=2*pi+asin(d);
return ddd;
}


double myAngleP(double nx,double ny)
{
 double  d,dd,ddd,pi;
pi=atan(1.)*4;
d=ny;
dd=nx;
ddd=sqrt(d*d+dd*dd);
d=d/ddd;
dd=dd/ddd;
if((d>=0)&&(dd>=0)) ddd=asin(d);
if((d>=0)&&(dd<0)) ddd=pi-asin(d);
if((d<0)&&(dd<0)) ddd=pi-asin(d);
if((d<0)&&(dd>=0)) ddd=2*pi+asin(d);
return ddd;
}


