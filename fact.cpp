#pragma once
#include "stdafx.h"
#include "math.h"
#include "Form1.h"
#include "fact.h"

//������ ������ ���������. � �������� ������ ������ ����� (xi, yi)
int fact(int xm[25], int ym[25])
{
	// ��� ������������
	const int nmbmax = 25; //������������ ����� ����� �������
	const int nmbp = 4; //����� ������ ��������������, ��������� �����
	double Pi = atan(1.) * 4;
	Polyline *z; // ���������� �����, �������������� ��������
	Polyline p; //���������� ������ ��������������, ��������� �����

	Parameters parameters;
	parameters.alf = 1; // ������������ ���� ��������
	parameters.rmax = 100; //�����������-���������� ����� �������
	parameters.rmin = 3; //����������� ���������� ����� ���������
	parameters.initialRange = { { -1, 1 },{ -1, -1 } };
	parameters.initialVec = { { 4.5, 5.0 },{ -1, 0 } };
	parameters.finalVec = { { 2,4 } ,{ -1, 1 } };

	//������ ������ ������ ,�������������� ��������� �������
	Point2d tempP[nmbp] = { { 0,0 },{ 10,0 },{ 10,10 },{ 0,10 } };
	p = { nmbp, tempP }; //�������������� ��� �������

						 //��������� ��������, �������������� �����������
	z = EndSelect(parameters, p);

	//������������ � ������ ����
	for (int k = 0; k < z->vertex_count; k++)
	{
		xm[k] = floor(z->vertex[k].x * 150);
		ym[k] = floor(900 - z->vertex[k].y * 150);
	}
	return z->vertex_count;
}

/*
������� ������������� ��� ����������� ������� �������� ���������� ����������� ��� ����������� �����
�������� ����� ������� �������� � ������ ��������� �������� ��� �������:
- ���� �������� < alf;
- ����� ������� ����� ������� �������� > rmin.
��������� -�����  ����� �������� � ������ �������� ������� �������� � ������ ��������� ��������

����������  ���������.
��� ����� :
parameters - ��������� ��� ������ ���������
nmbmax -�����������-���������� ����� ����� �������
p.vertex[i] -���������� ������ ��������������, ��������� �����
nmbp -����� ������ ��������������, ��������� �����
��� ������
zxy.vertex[i] - ���������� ����� �������� ������� ��������
(����� ���������� ������� � ������ ������� �� �� �����).
zxy.vertex[0] - ���������� ������ �������� �������
...
zxy.vertex[nmb-1]-���������� ����� ��������� �������
��� zxy.vertex_cont - ����� ����� ����� �������, ������� ����� ������ � �����.
*/
//TODO
//VariantParameters.polylineLength (rl) ����� �� ������������, �� ������������� � �� ��������, � ������ �������� �� ������������
//?�������� ������� ���������� ��������?
Polyline* EndSelect(Parameters parameters, Polyline &p)
{
	const int maxVertexCount = 25;
	int choosenVariant; //��������� ������� ����������

						//VariantParameters currentVarParameters;
						//VariantParameters varParameters[5];

	Polyline* zxy;

	choosenVariant = 4;

	zxy = EndMarsh(parameters, choosenVariant);

	return zxy;
}

/*
������� ������������� ��� ����������� ����� �������� ����� ������� �������� � ������ ��������� �������� ��� �������:
- ���� �������� < alf;
- ����� ������� ����� ������� �������� > rmin.
��������� -�����  ����� �������� ����� ������� �������� � ������ ��������� ��������

���������� ���������.
��� ����� :
parameters - ��������� ��� ������ ���������
choosenVariant - ����� �������� �����������
zxy.vertex_count -�����������-���������� ����� ����� �������

��� ������
zxy.vertex[i] - ���������� ����� �������� � ������� i
(����� ���������� ������� � ������ ������� �� �� �����).
��� ���� :
zxy.vertex[0]  - ���������� ������ �������� �������
...
zxy.vertex[nmb-1] -���������� ����� ��������� �������
��� zxy.vertex_count - ����� ����� ����� �������, ������� ����� ������ � �����.
rlen - ����� �������
ipar -
ipar[0] - ������� ������������� �������
ipar[1] - ����� ����� ����� �������
ipar[2] - ������� ������� �����
ipar[3] - ������� ������� ������� � ������ �������,��� rmin
*/
//TODO ipar �������� �� VariantParameters
//������ �������
Polyline* EndMarsh(Parameters param, int choosenVariant)
{
	Point2d kn, kf, on, of;
	Point2d nVec[10], fVec[10];
	double r, rr, fin, fout, Pi, alf, beta, gamm;
	int inds, nmb, indr;
	int i;
	Pi = atan(1.) * 4;

	//���-�� �����
	double dmin, dmax, fi[10], psi[10], d, dd, rmin;
	int n, ni, nii, iz1, iz2, ind, fk, fkk, k;


	alf = Pi / param.rmin;
	rmin = param.alf;


	//� ���� ���� ���. �������������. ����������
	inds = 0;

	Point2d dnArr[4];
	double dArr[4];




	n = 2 * Pi / alf + 0.00000000001;
	dmin = myAngleP(param.initialRange.begin);
	dmax = myAngleP(param.initialRange.end);

	Polyline* zxy = new Polyline();
	zxy->vertex = new Point2d[10];

	nVec[0] = param.initialVec.begin;// ������ � ���� �������� �������
	fin = myAngleP(param.initialVec.end);
	fVec[0] = param.finalVec.begin; // ����� � ���� ���������� �������
	fout = myAngleP(param.finalVec.end);

	//�������������� � ��������� ������ ������
	zxy->vertex_count = 25;
	zxy->vertex = new Point2d[zxy->vertex_count];
	for (i = 0; i<zxy->vertex_count; i++)
	{
		zxy->vertex[i] = { 0, 0 };
	}



	//����������� ����������-������
	r = rmin / (2 * sin(alf / 2));
	//����������� ���������� - ����������� ������
	if (choosenVariant == 1) {
		iz1 = 1;
		iz2 = 1;
	}
	if (choosenVariant == 2) {
		iz1 = 1;
		iz2 = -1;
	}
	if (choosenVariant == 3) {
		iz1 = -1;
		iz2 = 1;
	}
	if (choosenVariant == 4) {
		iz1 = -1;
		iz2 = -1;
	}

	//����������� ���������� - ������
	gamm = fin + (Pi*0.5 - alf / 2)*iz1;
	gamm = AngleCor(gamm);
	fi[0] = gamm + Pi;
	fi[0] = AngleCor(fi[0]);
	on.x = nVec[0].x + r*cos(gamm);
	on.y = nVec[0].y + r*sin(gamm);
	gamm = fout + (Pi*0.5 + alf / 2)*iz2;
	gamm = AngleCor(gamm);
	psi[0] = gamm + Pi;
	psi[0] = AngleCor(psi[0]);
	of.x = fVec[0].x + r*cos(gamm);
	of.y = fVec[0].y + r*sin(gamm);

	// ���������� ���
	for (i = 0; i<n - 1; i++)
	{
		fi[i + 1] = fi[0] + iz1*(i + 1)*alf;
		fi[i + 1] = AngleCor(fi[i + 1]);
		nVec[i + 1].x = on.x + r*cos(fi[i + 1]);
		nVec[i + 1].y = on.y + r*sin(fi[i + 1]);
		psi[i + 1] = psi[0] - iz2*(i + 1)*alf;
		psi[i + 1] = AngleCor(psi[i + 1]);
		fVec[i + 1].x = of.x + r*cos(psi[i + 1]);
		fVec[i + 1].y = of.y + r*sin(psi[i + 1]);
	}

	//����������� ����� �������
	gamm = myArg(on, of);
	if ((choosenVariant == 2) || (choosenVariant == 3))
	{
		rr = sqrt((of.x - on.x)*(of.x - on.x) + (of.y - on.y)*(of.y - on.y));
		if (rr<2 * r)
		{
			nmb = -1;
			zxy->vertex_count = nmb;
			return zxy;
		}
		else
		{
			beta = asin((2 * r) / rr);
			d = gamm + iz1*beta + iz2*Pi*0.5;
			d = AngleCor(d);
			dd = gamm + iz1*beta - iz2*Pi*0.5;
			dd = AngleCor(dd);
		}
	}
	if ((choosenVariant == 1) || (choosenVariant == 4))
	{
		d = -Pi*0.5*iz1 + gamm;
		d = AngleCor(d);
		dd = d;
	}
	kn.x = on.x + r*cos(d);
	kn.y = on.y + r*sin(d);
	kf.x = of.x + r*cos(dd);
	kf.y = of.y + r*sin(dd);

	//����
	for (int i = 0; i < 3; ++i) {
		dArr[i] = AngleCor(d - 0.5*alf);
		dnArr[i].x = on.x + r*cos(dArr[i]);
		dnArr[i].y = on.y + r*sin(dArr[i]);
	}
	dArr[3] = AngleCor(d - alf);
	dnArr[3].x = on.x + r*cos(dArr[3]);
	dnArr[3].y = on.y + r*sin(dArr[3]);

	//����������� ������� �����, �������� � ����
	//��������� ���
	ind = 0;
	i = 0;
	do {
		d = fi[i] - dArr[0];
		d = AngleCor(d);
		dd = fi[i] - dArr[1];
		dd = AngleCor(dd);
		if ((dd>Pi / 2) && (d <= Pi / 2)) {
			ni = i;
			ind = 1;
		}
		else {
			i = i + 1;
			if (i == (n + 1)) ind = 1;
		}
	} while (ind == 0);

	//�������� ���
	ind = 0;
	i = 0;
	do {
		d = psi[i] - dArr[2];
		d = AngleCor(d);
		dd = psi[i] - dArr[3];
		dd = AngleCor(dd);
		if ((dd>Pi / 2) && (d <= Pi / 2)) {
			fk = i;
			ind = 1;
		}
		else {
			i = i + 1;
			if (i == (n + 1)) ind = 1;
		}
	} while (ind == 0);

	//����������� ����� ������� ���
	// ni>0 fk>1
	if ((ni>0) && (fk>1)) {
		nii = ni - 1;
		fkk = fk - 1;
		if (iz1 == 1)
			ind = PointLeft(fVec[fk], nVec[ni], nVec[nii]);
		else
			ind = PointRight(fVec[fk], nVec[ni], nVec[nii]);
		if (ind == 1)
			ni = nii;
		else {
			if (iz1 == 1)
				ind = PointRight(fVec[ni], nVec[fk], nVec[fkk]);
			else
				ind = PointLeft(fVec[ni], nVec[fk], nVec[fkk]);
			if (ind == 1)
				fk = fkk;
		}
	}

	//
	if ((ni > 0) && (fk < 2))
	{
		nii = ni - 1;
		if (iz1 == 1)
			ind = PointLeft(fVec[fk], nVec[ni], nVec[nii]);
		else
			ind = PointRight(fVec[fk], nVec[ni], nVec[nii]);
		if (ind == 1)
			ni = nii;
		fk = 1;
		ind = AngleCheck(alf, nVec[ni - 1], nVec[ni], fVec[1]);
		inds = inds + ind;
		ind = AngleCheck(alf, nVec[ni], fVec[1], fVec[0]);
		inds = inds + ind;
		if (inds > 0) {
			zxy->vertex_count = inds; //TODO ���������
			return zxy;
		}
	}


	// ni=0 fk<2
	if ((ni == 0) && (fk<2)) {
		fk = 1;
		ind = ZAngleCheck(dmax, dmin, nVec[0], fVec[fk]);
		inds = inds + ind;
		if (inds>0)
			inds = 3;
		if (inds = 0) {
			ind = AngleCheck(alf, nVec[ni], fVec[1], fVec[0]);
			inds = inds + ind;
			if (inds>0)
				inds = 2;
		}
	}
	// ni=0 fk>1
	if ((ni == 0) && (fk>1))
	{
		fkk = fk - 1;
		if (iz2 == 1)
			ind = PointRight(nVec[ni], fVec[fk], fVec[fkk]);
		else
			ind = PointLeft(nVec[ni], fVec[fk], fVec[fkk]);

		if (ind == 1)
			fk = fkk;
		ind = ZAngleCheck(dmax, dmin, nVec[0], fVec[fk]);
		ind = inds + ind;
		if (inds>0)
			inds = 3;
	}

	// ������������ ������ ������ ��������
	for (k = 0; k<ni + 1; k++) {
		zxy->vertex[k] = nVec[k];
	}

	for (k = 0; k<fk + 1; k++) {
		zxy->vertex[ni + k + 1] = fVec[fk - k];
	}
	nmb = ni + fk + 2;
	dd = MyMod(nVec[ni], fVec[fk]);


	if (dd<rmin)
		inds = 4;
	else
		inds = myLoop(nVec, fVec, ni, fk);
	if (inds == 0) {
		zxy->vertex_count = nmb;
		return zxy;
	}
	else {
		zxy->vertex_count = -inds;
		return zxy;
	}
}


//��������� ????
int RegBoundary(Polyline &zxy, Polyline &p, int indreg)
{
	double d1, d2, Pi;
	Pi = atan(1.) * 4;
	indreg = 0;
	for (int i = 0; i < zxy.vertex_count; i++)
	{
		for (int k = 0; i<p.vertex_count - 1; i++)
		{
			d1 = myArg(zxy.vertex[i], p.vertex[k]);
			d2 = myArg(zxy.vertex[i], p.vertex[k + 1]);
			d2 = d2 - d1;
			if (d2<0) d2 = d2 + 2 * Pi;  //������ ���, ����� ���� ��� ������������
			if (d2 >= Pi) indreg = indreg + 1; //���� ���� ������, ��� Pi, �� indreg++
		}
		//��������� �������������� ������
		d1 = myArg(zxy.vertex[i], p.vertex[p.vertex_count - 1]); //���� ����� Ni � Pn-1
		d2 = myArg(zxy.vertex[i], p.vertex[0]); //���� ����� Ni � P0
		d2 = d2 - d1;
		if (d2<0)d2 = d2 + 2 * Pi;
		if (d2 >= Pi)indreg = indreg + 1;
	}
	return indreg;
}
//��������� ���� �������� ����� ����� �������
double myArg(Point2d &n1, Point2d &f1)
{
	double  d, dd, ddd, pi;
	pi = atan(1.) * 4;
	d = f1.y - n1.y;
	dd = f1.x - n1.x;
	ddd = sqrt(d*d + dd*dd); //���������� ����� ����� �������
	d = d / ddd; //cos
	dd = dd / ddd; //sin
	if ((d >= 0) && (dd >= 0)) ddd = asin(d);
	if ((d >= 0) && (dd<0)) ddd = pi - asin(d);
	if ((d<0) && (dd<0)) ddd = pi - asin(d);
	if ((d<0) && (dd >= 0)) ddd = 2 * pi + asin(d);
	return ddd;
}


////������ ������� (���������� ����� ����� �������) (nx,ny)-������,(fx,fy)-�����)
//double myMod(Point2d &n1, Point2d &f1)
//{
//    double d, dd, ddd;
//    d = f1.y - n1.y;
//    dd = f1.x - n1.x;
//    ddd = sqrt(d*d + dd*dd);
//    return ddd;
//}

double myAngle(Point2d &n1, Point2d &f1)
{
	double d, dd, ddd, pi;
	pi = atan(1.) * 4;
	d = n1.y - f1.y;
	dd = n1.x - f1.x;
	ddd = sqrt(d*d + dd*dd);
	d = d / ddd;
	dd = dd / ddd;
	if ((d >= 0) && (dd >= 0)) ddd = asin(d);
	if ((d >= 0) && (dd<0)) ddd = pi - asin(d);
	if ((d<0) && (dd<0)) ddd = pi - asin(d);
	if ((d<0) && (dd >= 0)) ddd = 2 * pi + asin(d);
	return ddd;
}

double myAngleP(Point2d &n1)
{
	double d, dd, ddd, pi;
	pi = atan(1.) * 4;
	d = n1.y;
	dd = n1.x;
	ddd = sqrt(d*d + dd*dd);
	d = d / ddd;
	dd = dd / ddd;
	if ((d >= 0) && (dd >= 0)) ddd = asin(d);
	if ((d >= 0) && (dd<0)) ddd = pi - asin(d);
	if ((d<0) && (dd<0)) ddd = pi - asin(d);
	if ((d<0) && (dd >= 0)) ddd = 2 * pi + asin(d);
	return ddd;
}

double AngleCor(double dd)
{
	double d, pi;
	pi = atan(1.) * 4;
	d = 2 * pi;
	if (dd >= d) dd = dd - d;
	if (dd<0) dd = dd + d;
	return dd;
}

int PointLeft(Point2d nxy, Point2d fxy, Point2d fab)
//���� ������� (nx,ny)-������,(fx,fy)-�����)
//���� ����� (fx,fy) ����� ����� (fa,fb) �� Result=0
{
	double d, dd, pi;
	pi = atan(1.) * 4;
	d = myArg(nxy, fxy);
	dd = myArg(nxy, fab);
	dd = d - dd;
	dd = AngleCor(dd);
	if (dd<pi) return 0;
	else return 1;
}


int PointRight(Point2d nxy, Point2d fxy, Point2d fab)
//���� ������� (nx,ny)-������,(fx,fy)-�����)
//���� ����� (fx,fy) ������ ����� (fa,fb) �� Result=0
{
	double d, dd, pi;
	pi = atan(1.) * 4;
	d = myArg(nxy, fxy);
	dd = myArg(nxy, fab);
	dd = d - dd;
	dd = AngleCor(dd);
	if (dd>pi) return 0;
	else return 1;
}

int MyLine(Point2d nxy1, Point2d nxy2, double a[2])
//  (nx1,ny1) � (nx2,ny2)-���������� �����, �������� �����
// a[0],a[1] �����. ��������� �����
// ind=1 - ����������� ������ x=a
{
	a[0] = nxy2.x - nxy1.x;
	if (fabs(a[0])>0.0000000000001)
	{
		a[1] = (nxy1.y*nxy2.x - nxy2.y*nxy1.x) / a[0];
		a[0] = (nxy2.y - nxy1.y) / a[0];
		return 0;
	}
	else
	{
		a[0] = nxy1.x;
		return 1;
	}
}



int LineCross(double a, double b, double c, double d, double e[2])
//  (e[0],e[1] -���������� ����� ����������� �����
// a,b  � c,d  -�����. ���������, �������������� �����
// ind=1 - ����� ����������� ������ ����� N1, �.�. �����
// ����������� ������ �� ������ ������� �����, �� � ���� ����� ��������� ���
{
	e[0] = a - c;
	if (fabs(e[0])>0.0000000000001)
	{
		e[1] = (a*d - b*c) / e[0];
		e[0] = (d - b) / e[0];
		return 0;
	}
	else
		// ����� ����������� - ����������� ���
		return 1;
}

int AngleCheck(double dm, Point2d nxy, Point2d fxy, Point2d fab)
// dd ���� ����� ��������� (N,F) � (F,Fb)
//����  |dd|<dm, �� return 0 
{
	double  pi, d, dd;
	pi = atan(1.) * 4;
	d = myArg(nxy, fxy);
	dd = myArg(fxy, fab);
	dd = dd - d;
	AngleCor(dd);
	d = 2 * pi - dm;
	if ((dd<dm) || (dd>d)) return 0;
	else return 1;
}

int ZAngleCheck(double dmax, double dmin, Point2d nxy, Point2d fxy)
// dd ���� ����� ��������� (N,F) � (F,Fb) ��������� � ������� (dmin,dmax)
{
	double d, dd;
	dd = myArg(nxy, fxy);
	d = dmax - dd;
	AngleCor(d);
	dd = dd - dmin;
	AngleCor(dd);
	if ((dd>0) && (d>0)) return 0;
	else return 1;
}


double MyMod(Point2d nxy, Point2d fxy)
//������ ������� (nx,ny)-������,(fx,fy)-�����)
{
	double  d, dd, ddd;
	d = fxy.y - nxy.y;
	dd = fxy.x - nxy.x;
	ddd = sqrt(d*d + dd*dd);
	return ddd;
}

int myLoop(Point2d nxy[10], Point2d fxy[10], int ni, int fk)
// a �����. ��������� �����- ������� �����
// c �����. ��������� �����- c��������� ��� ��������� �������
// e ���������� ����� �����������
// ind=5 - ����� �� ��������� ���
//ind1=1 ����� 1 �����������
//ind2=1 ����� 2 �����������
//ind3=1 ����� �����������
//ind4=1 ����� ����������� ������ N1
//ind5=1 ����� ����������� ������ � ������� (Nni,Ffk)
{
	double a[2], c[2], e[2], d1, d2;
	int ind1, ind2, ind3, ind4, ind5, ind;
	double pi;
	pi = atan(1.) * 4;
	ind1 = MyLine(nxy[ni], fxy[fk], a);
	//����������� ��������� ����� ����������� ����� ���������� ������� ������� �����
	ind2 = MyLine(nxy[0], nxy[1], c);
	ind2 = ind1 + 2 * ind2;
	ind3 = 0;
	if (ind2 == 3) ind3 = 1;
	if (ind2 == 2)
	{
		e[0] = c[0];
		e[1] = a[1] * c[0] + a[1];
	}
	if (ind2 == 1)
	{
		e[0] = a[0];
		e[1] = a[0] * c[0] + c[1];
	}
	if (ind2 == 0)
		ind3 = LineCross(a[0], a[1], c[0], c[1], e);
	// ��������� ����� ����������� �� ����� ���������� ������� (ind3=1 ����������� ���)
	ind4 = 0;
	d1 = MyMod(Point2d{ e[0], e[1] }, nxy[0]);
	d2 = MyMod(Point2d{ e[0], e[1] }, nxy[1]);
	if ((d1<0.0000000000001) || (d2<0.0000000000001)) ind4 = 1;
	if ((ind3 == 1) || (ind4 == 1)) ind = 0;
	else
	{
		d1 = myArg(nxy[0], nxy[1]);
		d2 = myArg(Point2d{ e[0], e[1] }, nxy[0]);
		d2 = d2 - d1;
		d2 = AngleCor(d2);
		if ((d2<0.1) || (d2>2 * pi - 0.1)) ind4 = 1;
		else ind4 = 0;
		// ��������� ����� ����������� �� ����� ������� c����
		d1 = myArg(Point2d{ e[0], e[1] }, nxy[ni]);
		d2 = myArg(Point2d{ e[0], e[1] }, fxy[fk]);
		d2 = d2 - d1;
		d2 = AngleCor(d2);
		if ((d2<0.1) || (d2>2 * pi - 0.1)) ind5 = 0;
		else ind5 = 1;
		ind = ind4 + ind5;
	}
	if (ind>0) return 5;
	//
	//����������� ��������� ����� ����������� ����� ��������� ������� � ������� �����
	ind2 = MyLine(fxy[0], fxy[1], c);
	ind2 = ind1 + 2 * ind2;
	ind3 = 0;
	if (ind2 == 3) ind3 = 1;
	if (ind2 == 2)
	{
		e[0] = c[0];
		e[1] = a[0] * c[0] + a[1];
	}
	if (ind2 == 1)
	{
		e[0] = a[0];
		e[1] = a[0] * c[0] + c[1];
	}
	if (ind2 == 0) ind3 = LineCross(a[0], a[1], c[0], c[1], e);
	// ��������� ����� ����������� �� ����� ��������� ������� (ind3=1 ����������� ���)
	ind4 = 0;
	d1 = MyMod(Point2d{ e[0], e[1] }, fxy[0]);
	d2 = MyMod(Point2d{ e[0], e[1] }, fxy[1]);
	if ((d1<0.0000000000001) || (d2<0.0000000000001)) ind4 = 1;
	if ((ind3 == 1) || (ind4 == 1)) ind = 0;
	else
	{
		d1 = myArg(fxy[0], fxy[1]);
		d2 = myArg(Point2d{ e[0], e[1] }, fxy[1]);
		d2 = d2 - d1;
		d2 = AngleCor(d2);
		if ((d2<0.1) || (d2>2 * pi - 0.1)) ind4 = 1;
		else ind4 = 0;
		// ��������� ����� ����������� �� ����� ������� c����
		d1 = myArg(Point2d{ e[0], e[1] }, nxy[ni]);
		d2 = myArg(Point2d{ e[0], e[1] }, fxy[fk]);
		d2 = d2 - d1;
		d2 = AngleCor(d2);
		if ((d2<0.1) || (d2>2 * pi - 0.1)) ind5 = 0;
		else ind5 = 1;
		ind = ind4 + ind5;
	}
	if (ind>0) return 6;
	return 0;
}