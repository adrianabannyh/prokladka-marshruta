#pragma once
#include "stdafx.h"

// ����� �� ���������
struct Point2d
{
	double x;//���������� �� X
	double y;//���������� �� Y
};
// ������������� �� ��������� 
struct Polyline
{
	int vertex_count; //����� ������ � �������������� 
	Point2d* vertex; //������� ��������������, ��� ������ ������ 
};

struct Vector2d
{
	Point2d begin; //���������� ������ �������
	Point2d end; //���������������� ����������� �������
};

//��������� ��� ������ ���������
struct Parameters
{
	Vector2d initialVec; //��������� ������
	Vector2d finalVec;  //�������� ������
	Vector2d initialRange; //������� ���������� �������� ����������� �������� �������
	double rmin;//rmin - ����������� ���������� ����� ������� ��������
	double rmax;//rmax - ����������� - ���������� ����� �������
	double alf; //alf - ������������ ���� ��������
};

//�������������� �������� �������� ������� //TODO ��� �������� 0, � ��� 1?? true ��� false?
struct VariantParameters
{
	int isLoop; //������� ������� ����� //iloop
	int isSolutionExist; //���������� �� ������� //iex
	int vertexCount; //���������� ������ � ������� //inmb
	int ireg; //���������� ����-�� TODO
	int isSmallR; //�������, ���� �� �������, ������� rmin //ir
	double polylineLength; //rl
};



int fact(int xm[25], int ym[25]);
Polyline* EndSelect(Parameters parameters, Polyline &p);
Polyline* EndMarsh(Parameters param, int choosenVariant);
int RegBoundary(Polyline &zxy, Polyline &p, int indreg);
double myArg(Point2d &n1, Point2d &f1);
double myAngle(Point2d &n1, Point2d &f1);
double myAngleP(Point2d &n1);
double AngleCor(double dd);
int PointLeft(Point2d nxy, Point2d fxy, Point2d fab);
int PointRight(Point2d nxy, Point2d fxy, Point2d fab);
double MyMod(Point2d nxy, Point2d fxy);
int myLoop(Point2d nxy[10], Point2d fxy[10], int ni, int fk);
int ZAngleCheck(double dmax, double dmin, Point2d nxy, Point2d fxy);
int AngleCheck(double dm, Point2d nxy, Point2d fxy, Point2d fab);
int LineCross(double a, double b, double c, double d, double e[2]);
int MyLine(Point2d nxy1, Point2d nxy2, double a[2]);
